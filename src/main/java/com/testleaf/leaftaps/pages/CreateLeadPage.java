package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage() {}
	
	
	public CreateLeadPage enterCname() {
		
		clearAndType(locateElement("Id","createLeadForm_companyName"), "Equiniti");
		
		return this;
	}
	
	
	public CreateLeadPage enterFname() {
		
		clearAndType(locateElement("Id", "createLeadForm_firstName"), "FirstName");
		
		return this;
	}
	
	public CreateLeadPage enterLname() {
		
		clearAndType(locateElement("Id", "createLeadForm_lastName"), "LastName");
		
		return this;
	}
	
	public ViewLeadPage clickCreateLead() {
		
		click(locateElement("Name", "submitButton"));
		
		return new ViewLeadPage();
		
	}
	
	

}
