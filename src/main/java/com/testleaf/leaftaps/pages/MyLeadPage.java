package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadPage extends Annotations {
	
	public MyLeadPage() {}
	
	public CreateLeadPage clickCreateLead() {
		
		click(locateElement("LinkText", "Create Lead"));
		
		return new CreateLeadPage();
	}

}


