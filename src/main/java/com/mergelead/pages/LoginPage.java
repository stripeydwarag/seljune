package com.mergelead.pages;

import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations{
	
	public LoginPage() {}
	
	public LoginPage enterUserName() {
		
		clearAndType(locateElement("id", "username"), "Demosalesmanager");
		
		return this;
		
	}
	
	public LoginPage enterPassword() {
		
		clearAndType(locateElement("id", "password"), "crmsfa");
		
		return this;
	}
	
	public HomePage clickLogin() {
		
		click(locateElement("ClassName", "decorativeSubmit"));
		
		return new HomePage();
		
	}
}
