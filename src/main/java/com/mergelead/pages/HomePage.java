package com.mergelead.pages;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations{
	
	public HomePage() {}
	
	
	public MyHomePage clickCRMSFA() {
		
		click(locateElement("LinkText", "CRM/SFA"));
		
		return new MyHomePage();
	}

}


