package com.mergelead.pages;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadPage extends Annotations{
	
	public MyLeadPage() {}
	
	
	public MergeLeadPage clickMergeLead() {
		
		click(locateElement("LinkText", "Merge Leads"));
		
		return new MergeLeadPage();
		
	}

}
