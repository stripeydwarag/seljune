package com.mergelead.pages;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations{
	
	public MyHomePage() {}
	
	public MyLeadPage clickLead() {
		
		click(locateElement("LinkText", "Leads"));
		
		return new MyLeadPage();
		
	}
	

}
