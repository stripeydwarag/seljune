package com.mergelead.pages;

import com.autoBot.testng.api.base.Annotations;

public class MergeLeadPage extends Annotations{
	
	public MergeLeadPage() {}
	
	public MergeLeadPage clickFromLeadIcon() {
		
		click(locateElement("XPath", "(//img[@alt='Lookup'])[1]"));
		
		return this;
		
	}

	public MergeLeadPage moveToNewWindow() {
		
		switchToWindow("Find Leads");
		
		return this;
		
	}
	
	public MergeLeadPage enterLeadId() {
		
		clearAndType(locateElement("Name", "id"), "10071");
		
		return this;
	}
	
	public MergeLeadPage clickFindLead() {
		
		click(locateElement("Xpath", "(//button[@class='x-btn-text'])[1]"));
		
		return this;
	}
	
	public MergeLeadPage clickResultLead() {
		
		click(locateElement("XPath", "(//a[@class='linktext'])[1]"));
		
		return this;
	}
	
	
}
